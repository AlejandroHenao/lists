const RitaTypes=[
        {
            tag:'cc',
            description:'coordinating conjunction'
        },
        {
            tag:'cd',
            description:'cardinal number'
        },
        {
            tag:'dt',
            description:'determiner'
        },
        {
            tag:'ex',
            description:'existential there'
        },
        {
            tag:'fw',
            description:'foreing word'
        },
        {
            tag:'in',
            description:'preposition or subordinating conjucntion'
        },
        {
            tag:'jj',
            description:'adjetive'
        },
        {
            tag:'jjr',
            description:'adjetive, comparative'
        },
        {
            tag:'jjs',
            description:'adjetive, superlative'
        },
        {
            tag:'ls',
            description:'list item marker'
        },
        {
            tag:'md',
            description:'modal'
        },
        {
            tag:'nn',
            description:'noun, singular or mass'
        },
        {
            tag:'nns',
            description:'noun,plural'
        },
        {
            tag:'nnp',
            description:'proper noun, singular'
        },
        {
            tag:'nnps',
            description:'proper noun,plural'
        },
        {
            tag:'pdt',
            description:'predeterminer'
        },
        {
            tag:'pos',
            description:'possessive ending'
        },
        {
            tag:'prp',
            description:'personal pronoun'
        },
        {
            tag:'prp$',
            description:'posessive pronoun'
        },
        {
            tag:'rb',
            description:'adverb'
        },
        {
            tag:'rbr',
            description:'adverb, comparative'
        },
        {
            tag:'rbs',
            description:'adverb, superlative'
        },
        {
            tag:'rp',
            description:'particle'
        },
        {
            tag:'sym',
            description:'symbol'
        },
        {
            tag:'to',
            description:'to'
        },
        {
            tag:'uh',
            description:'interjection'
        },
        {
            tag:'vb',
            description:'verb, base form'

        },
        {
            tag:'vbd',
            description:'verb, past tense'
        },
        {
            tag:'vbg',
            description:'verb, gerund or present participle'
        },
        {
            tag:'vbn',
            description:'verb, past participle'
        },
        {
            tag:'vbp',
            description:'verb, non-3rd person singular present'
        },
        {
            tag:'vbz',
            description:'verb, 3rd person singular present'
        },
        {
            tag:'wdt',
            description:'wh-determiner'
        },
        {
            tag:'wp',
            description:'wh-pronoun'
        },
        {
            tag:'wp$',
            description:'possesive wh-pronoun'
        },
        {
            tag:'wrb',
            description:'wh-adverb'
        },
        {
            tag:'all',
            description:'all'
        },


];

export default RitaTypes
